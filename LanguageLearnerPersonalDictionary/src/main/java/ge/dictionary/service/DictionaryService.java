package ge.dictionary.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Misha on 21. 02. 16.
 */
@Service
public class DictionaryService {

    @Autowired
    @PersistenceContext(unitName = "dictionary_pu")
    EntityManager em;

    private static final Logger LOGGER = LoggerFactory.getLogger(DictionaryService.class);

    public String return1() {
        return em.createNativeQuery("VALUES CURRENT_TIMESTAMP ").getSingleResult().toString();
    }


}
