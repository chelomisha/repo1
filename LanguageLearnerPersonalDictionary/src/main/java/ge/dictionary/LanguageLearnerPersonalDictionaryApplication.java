package ge.dictionary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanguageLearnerPersonalDictionaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LanguageLearnerPersonalDictionaryApplication.class, args);
	}
}
