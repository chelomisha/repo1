package ge.dictionary.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Misha on 08. 03. 16.
 */
@Entity
@Table(name = "WORD", schema = "DICTIONARY", uniqueConstraints = @UniqueConstraint(columnNames = {"VALUE", "LANG_ID"}))
public class Word implements Serializable {
    private Long id;
    private String value;
    private Language language;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Column(name = "VALUE", nullable = false)
    public String getValue() {
        return value;
    }


    @ManyToOne(targetEntity = Language.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "LANG_ID")
    public Language getLanguage() {
        return language;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
