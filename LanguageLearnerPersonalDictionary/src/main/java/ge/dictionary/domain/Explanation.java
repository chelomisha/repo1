package ge.dictionary.domain;

import ge.dictionary.model.ExplanationType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Misha on 09. 03. 16.
 */

@Entity
@Table(name = "EXPLANATION", schema = "DICTIONARY")
public class Explanation implements Serializable {

    private Long id;
    private String value;
    private ExplanationType type;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Column(name = "VALUE", nullable = false, unique = true)
    public String getValue() {
        return value;
    }

    @Column(name = "TYPE")
    @Enumerated(EnumType.ORDINAL)
    public ExplanationType getType() {
        return type;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setType(ExplanationType type) {
        this.type = type;
    }
}
