package ge.dictionary.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Misha on 21. 02. 16.
 */

@Entity
@Table(name = "LANGUAGE", schema = "DICTIONARY")
public class Language implements Serializable {
    private Long id;
    private String code;
    private String name;
    private Boolean isNative;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Column(name = "CODE", nullable = false, unique = true)
    public String getCode() {
        return code;
    }

    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    @Column(name = "NATIVE_FLG")
    public Boolean getIsNative() {
        return isNative;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsNative(Boolean isNative) {
        this.isNative = isNative;
    }
}