package ge.dictionary.controller;

import ge.dictionary.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Misha on 21. 02. 16.
 */
@RestController
public class Controller {

    @Autowired
    DictionaryService service;


    @RequestMapping(value = "about", method = RequestMethod.GET)
    public String about() {
        return "<H2>This is</H2><H1>Language Learners Personal Dictionary</H1><H2>web application.</H2>" + "<br />" +
                "Wishing you great success over at learning foreign language" + "<br />" +
                "check connection: " + service.return1();
    }
}
