package ge.dictionary.configuration.data.jpa;


import org.hibernate.dialect.Dialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by Misha on 21. 02. 16.
 */

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "ge.dictionary")
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager",
        basePackages = "ge.dictionary")

public abstract class JpaBasicConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(JpaBasicConfiguration.class);

    protected String entityPackage = "ge.dictionary.domain";

    @Bean
    public abstract DataSource dataSource();

    protected abstract Class<? extends Dialect> getDatabaseDialect();

    protected Properties getJpaProperties() {
        return null;
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        logger.info("\n\n************ {} ************\n\n", getDatabaseDialect().getCanonicalName());
        logger.info("\n\n****** Scanning '{}' Packages for Entities ******\n\n", entityPackage);


        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setShowSql(true);
        vendorAdapter.setDatabasePlatform(getDatabaseDialect().getName());

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan(entityPackage);
        factory.setDataSource(dataSource());
        factory.setPersistenceUnitName("dictionary_pu");
        if (getJpaProperties() != null) {
            factory.setJpaProperties(getJpaProperties());
        }
        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return txManager;
    }

}



