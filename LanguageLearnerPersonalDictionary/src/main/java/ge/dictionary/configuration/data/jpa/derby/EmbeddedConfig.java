package ge.dictionary.configuration.data.jpa.derby;

import ge.dictionary.configuration.data.jpa.Hbm2ddlType;
import ge.dictionary.configuration.data.jpa.JpaBasicConfiguration;
import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.dialect.DerbyTenSevenDialect;
import org.hibernate.dialect.Dialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.Properties;

import static java.lang.Boolean.TRUE;
import static org.hibernate.cfg.AvailableSettings.*;
import static org.hibernate.ejb.AvailableSettings.NAMING_STRATEGY;

/**
 * Created by Misha on 21. 02. 16.
 */
@Configuration
public class EmbeddedConfig extends JpaBasicConfiguration {


    @Override
    @Primary
    @Bean(name = "dictionary", destroyMethod = "close")
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        dataSource.setUrl("jdbc:derby:target/database/dictionary;create=true");
//        dataSource.setUsername(getUser());
//        dataSource.setPassword(getPassword());
        dataSource.setValidationQuery("values 1");
        dataSource.setTestOnBorrow(true);
        dataSource.setTestOnReturn(true);
        dataSource.setTestWhileIdle(true);
        dataSource.setTimeBetweenEvictionRunsMillis(1800000);
        dataSource.setNumTestsPerEvictionRun(3);
        dataSource.setMinEvictableIdleTimeMillis(1800000);
        return dataSource;
    }

    @Override
    protected Class<? extends Dialect> getDatabaseDialect() {
        return DerbyTenSevenDialect.class;
    }


    @Override
    protected Properties getJpaProperties() {
        Properties properties = new Properties();
        properties.setProperty(HBM2DDL_AUTO, Hbm2ddlType.CREATE_DROP.toValue());
        properties.setProperty(GENERATE_STATISTICS, TRUE.toString());
        properties.setProperty(SHOW_SQL, TRUE.toString());
        properties.setProperty(FORMAT_SQL, TRUE.toString());
        properties.setProperty(USE_SQL_COMMENTS, TRUE.toString());
        properties.setProperty(NAMING_STRATEGY, ImprovedNamingStrategy.class.getName());

        properties.setProperty("hibernate.connection.charSet", "UTF-8");
        return properties;
    }
}
